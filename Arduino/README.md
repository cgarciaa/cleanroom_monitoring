# Arduino Setup

## Shield PCB Design

A shield is used to connect up to 4 SHT sensors and 6 NTC sensors. The NTC resistors are configurable among 1k, 10k and 100k Ohm. 

The schematics and PCB layout were originally designed using Eagle. I converted them to Kicad but have not verified them thoroughly. As of now, there are a few rule check errors due to unconnected nets, which do not seem to be real though.

## Arduino Firmware

The Arduino firmware has only been tested with Arduino Uno. In principle it's possible to adapt it for other boards. It has several features which can be configured either on the firmware or by sending a command whilst the Arduino is running:

 * Two possible running modes: send data on request or send data automatically every X seconds.
 * Configurable delay between automated data sends. Note there's a delay between sensors read-out request (increasing with the number of SHTs) and sending, which adds to the set delay.
 * Configurable sensors output. Enable or disable sensors, set resistors connected to each channel for the proper calculation of the resistance measured.
 
One option is only configurable by editing the firmware source:

 * Cleanroom name, for identification purposes, it is sent at the beginning of the data string: #define CLEANROOM "Cleanroom 1"

The SHT1x library can be obtained from the [Arduino Playground - Sensirion](http://playground.arduino.cc/Code/Sensirion).

## Commands for the read-out configuration

- "READ": tells the Arduino to send the current read-out data. Works in non free-run mode.
- "SETMODE[0|1]": mode 0 makes the Arduino wait for the READ command to send data, mode 1 sends data every wait_time milliseconds. Mode 0 is the default.
- "SETDELAYn": sets the wait_time to n seconds.
- "SETMASKssssnnnnnn": sets the read-out mask, enabling first each of the 4 SHTs (s) and then each of the 6 NTCs (n). Enable with 1, disable with 0. Default mask is 1000111111. Only the first SHT is enabled.
- "SETRn,R": sets the resistor in series with NTC n (0-5) to value R in Ohms.

## Arduino Modification

The image below shows where the RESET EN trace is on the Arduino Uno. This should be cut for best stability of the monitoring system.

![Arduino Uno Reset Disable](ArduinoUno_Reset_Disable.jpg)

