#!/usr/bin/env python
# coding=utf-8
#
# Monitoring of cleanrooms through Arduino+SHT reader with microcontroller.
# This scripts takes the data once and exits
#
# 2018-01-15 Carlos Garcia Argos (carlos.garcia.argos@cern.ch)
#
import serial
import io, time, re, subprocess, datetime
from serialdetect import *
from serialinterface import *
from nclib import *

# Send the individual values to the graphite interface
def arduino_to_graphite(text):
  data = text.split(",")
  cleanroomname = ''
  nc = netcat.Netcat(('127.0.0.1', 2003), verbose = False, retry = True)
  if data[0] == 'Room 03 051':
    cleanroomname = 'bonding_lab'
  if data[0] == 'Room 03 050':
    cleanroomname = 'middle_cleanroom'
  if data[0] == 'Room 03 046':
    cleanroomname = 'assembly_room'
    
  nc.send(cleanroomname + '.temperature1 ' + str(data[1]) + ' ' + str(time.time()) + '\n')
  nc.send(cleanroomname + '.humidity1 ' + str(data[2]) + ' ' + str(time.time()) + '\n')
  
  # When No Data is available from the particle counter, do not send it
  if 'N' not in data[len(data) - 2]:
    nc.send(cleanroomname + '.small_particles ' + data[len(data) - 2].rstrip() + ' ' + str(time.time()) + '\n')
  if 'D' not in data[len(data) - 1]:
    nc.send(cleanroomname + '.large_particles ' + data[len(data) - 1].rstrip() + ' ' + str(time.time()) + '\n')
  
def ftdi_to_graphite(cleanroom, data):
  cleanroomname = ''
  nc = netcat.Netcat(('127.0.0.1', 2003), verbose = False, retry = True)
  if cleanroom == 'CR1':
    cleanroomname = 'assembly_room'
  if cleanroom == 'CR2':
    cleanroomname = 'middle_cleanroom'
  if cleanroom == 'CR3':
    cleanroomname = 'bonding_lab'
    
  nc.send(cleanroomname + '.temperature2 ' + str(data[0]) + ' ' + str(time.time()) + '\n')
  nc.send(cleanroomname + '.humidity2 ' + str(data[1]) + ' ' + str(time.time()) + '\n')

####################
#                  #
# Main starts here #
#                  #
####################
devices = serialdetect()
arduino_list = get_arduinos(devices)
ftdi_list = get_ftdis(devices)

print "Found ", 
print len(arduino_list),
print " Arduinos:"
for item in arduino_list:
  print item
  
print "Found ", 
print len(ftdi_list),
print "FTDIs:"
for item in ftdi_list:
  print item
  ftdi_get_sht_values(item['devname'])


filename_arduino = 'CleanroomLog_Arduino.txt'
filename_ftdi = 'CleanroomLog_FTDI.txt'

# Open the serial connections to the Arduinos
serials = []
for i in range(len(arduino_list)):
  item = arduino_list[i]
  serials.append(arduino_open_port(item['devname']))

f_arduino = open(filename_arduino, 'a')
f_ftdi = open(filename_ftdi, 'a')
time_now = datetime.datetime.now()
now = str(time_now)

print "New readout:"
arduino_data = []
ftdi_data = []
for i in range(len(arduino_list)):
  arduino_data = arduino_get_data(serials[i]).rstrip()
  print arduino_data
  # Sometimes I get "Unknown command" responses...
  if (len(arduino_data) > 10) and ("Unknown" not in arduino_data):
    f_arduino.write(now)
    f_arduino.write("; ")
    f_arduino.write(arduino_data+'\n')
    arduino_to_graphite(arduino_data)
  
for i in range(len(ftdi_list)):
  item = ftdi_list[i]
  cleanroom = ftdi_get_cleanroom(item['devname'])
  ftdi_data = ftdi_get_sht_values(item['devname'])
  if len(ftdi_data) > 1:
    f_ftdi.write(now)
    f_ftdi.write("; ")
    f_ftdi.write(cleanroom)
    f_ftdi.write(": ")
    f_ftdi.write(ftdi_data[0])
    f_ftdi.write(", ")
    f_ftdi.write(ftdi_data[1]+'\n')
    ftdi_to_graphite(cleanroom, ftdi_data)




