#!/usr/bin/env python
# coding=utf-8
# Serial interface to Arduinos and microcontroller boxes (FTDI chips)
# 2018-01-16 Carlos García Argos (carlos.garcia.argos@cern.ch)
#
import serial, time

def ftdi_get_cleanroom(port):
  ser = serial.Serial(port, 115200, timeout = 2)
  ser.write("V\r")
  cleanroom = ser.readline().rstrip()
  ser.close
  
  return cleanroom

def ftdi_get_sht_values(port):
  SHT = [0, 0]
  ser = serial.Serial(port, 115200, timeout = 2)
  ser.write("T\r")
  SHT[0] = ser.readline().rstrip()
  ser.write("F\r")
  SHT[1] = ser.readline().rstrip()
  ser.close()
  
  return SHT

# Port should be kept open all the time to avoid serial reset of the Arduino
# Alternatively, cut the "RESET EN" bridge on the board
def arduino_open_port(port):
  ser = serial.Serial(port, 115200, timeout = 7)
  time.sleep(0.5)
  return ser

# Just return the whole string
def arduino_get_data(ser):
  time.sleep(0.1)
  ser.write("READ")
  time.sleep(0.5)
  ser.flush()
  return ser.readline()
