#!/usr/bin/env python
# coding=utf-8
# Detect USB devices as /dev/* (only one level after /dev)
# 2018-01-16 Carlos García Argos (carlos.garcia.argos@cern.ch)
#
import re, serial
import subprocess

# Check that we're talking to the SHT reader
# New microcontroller version has "CRn" in the return to the "V" command 
# to differentiate among cleanrooms (n = 1, 2, 3)
def ftdi_test(port):
  validport = False
  ser = serial.Serial(port, 115200, timeout = 1)
  ser.write("V\r")
  ret = ser.readline()
  if 'CR' in ret:
    validport = True
    
  return validport
  
  

def serialdetect():
  usbdevices = re.compile("(?P<devname>/\w+/\w+)\s-\s(?P<name>\w+)$", re.I)
  com = subprocess.check_output("./serialdetect.sh")

  devs = []

  for a in com.split('\n'):
    if a:
      a = a.replace('.', '_')  # Dots mess up the regular expression and they sometimes are there...
      info = usbdevices.match(a)
      if info:
        dinfo = info.groupdict()
        devs.append(dinfo)
  
  return devs        

# Extract matching name substring from the list
def get_something(inputdevs, substr):
  outputdevs = []
  
  for item in inputdevs:
    if substr in item['name']:
      outputdevs.append(item)
  
  return outputdevs
  
# Extract the arduino devices from the list
def get_arduinos(inputdevs):
  return get_something(inputdevs, 'Arduino')

# Extract (valid) FTDI devices from the list
# Each chip creates two devices, it seems the lowest ID one from the pair is the one to talk to...
# Does a test and if it returns the valid "SHT" string, then add it to the list
def get_ftdis(inputdevs):
  working_ftdis = []
  all_ftdis = get_something(inputdevs, 'FTDI_Dual_RS232')
  
  for item in all_ftdis:
    if ftdi_test(item['devname']):
      working_ftdis.append(item)
  
  return working_ftdis
