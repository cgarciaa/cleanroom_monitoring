# Cleanroom Monitoring System
Monitoring of cleanrooms at the University of Freiburg: particle counts, temperature and humidity.

The system is based on a Rapberry Pi connected to Arduino boards and microcontrollers reading out SHT sensors. The system in the Raspberry Pi is raspbian running Grafana for data display. It boots automatically the required services and it starts the X server with a chromium instance accessing one of the Grafana dashboards that we have set up.

There are two possibilities when it comes to connecting and reading the Arduinos:

 * Use the script that opens the connection at the beginning and keeps it open. 
   * Pro: no hardware modifications to the Arduino board are required.
   * Con: if one of the Arduinos gets disconnected, the script crashes.
   
 * Use the script that opens the connection, reads out the values and closes it. 
   * Pro: stable, immune to Arduino disconnections or failures.
   * Con: you need to cut the "RESET EN" trace on Arduino Uno, to keep the Arduino from resetting on serial connect. This also changes the way they are programmed, so cut it after flashing the firmware.

