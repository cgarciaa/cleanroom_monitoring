'regfile = "m168pdef.dat"                                   'Atmega 168
$regfile = "m8def.dat"                                      'Atmega 8
$crystal = 8000000                                          'Quarzfrequenz
$baud = 115200                                              'RS232 Geschwindigkeit
$hwstack = 40                                               ' default use 32 for the hardware stack
$swstack = 30                                               'default use 10 for the SW stack
$framesize = 40                                             'default use 40 for the frame space


'-------------------------
'
'Steuert einen SHT11 (10Pin Anschluss) und einen SHT71 (4Pin 1.27 mm Buchse) an
'Beide Sensoren werden alle 500 ms ausgelesen und die Werte auf dem Display angezeigt
'
'Die Temperatur wird mit 'T' ausgelesen, die Feuchte mit 'F'. Wenn beide Sensoren angeschlossen sind, dann wird der SHT71 Sensor bevorzugt (nur dessen Werte an den PC gesendet)
'Mit T1 bzw. F1 und T2 bzw. F2 k�nnen die Sensoren getrennt ausgelesen werden (Alle COM-Befehle sind am Ende dieses Dokuments aufgelistet)
'
'-------------------------


'display muss noch gefixt werden (suche nach "todo")






'Watchdog
Config Watchdog = 2048
Start Watchdog                                              'todo

'**-- 1wire Definitionen -------
   1wirepin Alias Porta.7
   Config 1wire = 1wirepin

'**-- LCD Definitionen -------
   Lcd_e Alias Portd.5
   Lcd_rs Alias Portd.6
   Lcd_d4 Alias Portb.0
   Lcd_d5 Alias Portb.3
   Lcd_d6 Alias Portb.4
   Lcd_d7 Alias Portb.5
   Dim Lcdbyte As Byte
   Config Lcd = 20 * 4a
   Config Lcdpin = Pin , Db4 = Lcd_d4 , Db5 = Lcd_d5 , Db6 = Lcd_d6 , Db7 = Lcd_d7 , E = Lcd_e , Rs = Lcd_rs

'Sensor variables
   Dim Kommfehler As Byte                                   'kommfehler f�r den SHT11 Bus
   Dim Kommfehler2 As Byte                                  'kommfehler f�r dne SHT71 Bus

'Allgemeine variablen
   Dim Laufvar1 As Bit
   Laufvar1 = 1
   Dim I As Byte


'*************************** externer SHT11 �ber Harting 10 Pin ************

   '**--QIS ----------------------

   Qis_clk Alias Portd.3
   Qis_sda_out Alias Portb.1
   Qis_sda_pull Alias Portb.1                               '1 pullup setzen
   Qis_sda_in Alias Pinb.1
   Qis_sda_dir Alias Ddrb.1                                 '0 eingang , 1 ausgang

   Config Qis_clk = Output

   'SHT11
   Const Sht_temp = &H03                                    'Read Temp
   Const Sht_rh = &H05                                      'Read Rh
   Const Sht_st_r = &H07                                    'Read Status
   Const Sht_st_w = &H06                                    'Write Status
   Const Sht_reset = &H1E                                   'Reset

   Dim Sht_temp_high As Byte , Sht_temp_low As Byte , Sht_crc As Byte , Sht_temp_out As Integer
   Dim Sht_temp_single As Single , Sht_humidity_single As Single , Sht_out_single As Single

   Dim I2c_error As Byte
   Dim T_string As String * 10
   Dim Format_t_string As String * 6
   Dim Format_rh_string As String * 6
   Dim Single_tmp_i As Single
   Dim M As Byte                                            'laufindex f�r i2c komm
   Dim Dac_data(24) As Byte

   '**--QIS ----------------------

   '** Timer f�r timeout detection
   Dim Timeout_var As Integer                               'pm 32000

   '** Sub Definitions
   Declare Sub Write_i2c_qis(byval Qis_byte As Byte)
   Declare Sub Read_i2c_qis(qis_byte As Byte)
   Declare Sub I2c_read_ack_qis(error As Byte)


'*************************** externer SHT71 �ber 4 Pin 1.27 mm Buchse (oder interner SHT11) ************

   '**--QIS2 ----------------------

   Qis_clk2 Alias Portb.7
   Qis_sda_out2 Alias Portb.6
   Qis_sda_pull2 Alias Portb.6                              '1 pullup setzen
   Qis_sda_in2 Alias Pinb.6
   Qis_sda_dir2 Alias Ddrb.6                                '0 eingang , 1 ausgang

   Config Qis_clk2 = Output

   Dim Format_t_string2 As String * 6                       'String f�r SHT71
   Dim Format_rh_string2 As String * 6                      'String f�r SHT71

   '** Alle anderen Variablen sollten von QIS verwendbar sein

   Declare Sub Write_i2c_qis2(byval Qis_byte2 As Byte)
   Declare Sub Read_i2c_qis2(qis_byte2 As Byte)
   Declare Sub I2c_read_ack_qis2(error2 As Byte)


   '** Liste der Befehle, die Sensoren auslesen
   'Read_qis_sht11
   'Read_qis_sht71


'*************************** zus�tzliche BfS Sensoren Ende *******




'------------ LCD richtig initialisieren (KS0073-Problem) ----------
   Lcdbyte = &B00101100 : Gosub Lcdwrite
   Lcdbyte = &B00001001 : Gosub Lcdwrite
   Lcdbyte = &B00101000 : Gosub Lcdwrite
   Lcdbyte = &B00000110 : Gosub Lcdwrite
   Lcdbyte = &B00001100 : Gosub Lcdwrite
   Cls
   Dim K As Word
   K = 0

'------------ Serial Interrupt
Enable Urxc                                                 'serieller inputbuffer (ein byte l�st int aus)
On Urxc Eingang Nosave
Dim Befehl As String * 1
Echo Off

'Print "Booting finished"
Enable Interrupts


   Do
      Reset Watchdog
      Disable Interrupts
      Gosub Read_qis_sht11
      Gosub Read_qis_sht71
      Enable Interrupts
      Gosub Lcd_grad
      For I = 1 To 5
         Waitms 100
         Reset Watchdog
      Next I
      K = K + 1
      If K > 600 Then
         Cls
         Stop Watchdog
         Config Watchdog = 16
         Start Watchdog
         Waitms 200
      End If
   Loop
   End                                                      'end program



Lcd_grad:

   '** der Ursprung in der zweiten Zeile ist 2,9
   If Kommfehler = 1 Then                                   'fehlerbehandlung SHT11
      Locate 2 , 9
      Lcd "    SHT11 NC      "
   Else
      Locate 2 , 10
      Lcd Format_t_string;
      Locate 2 , 16
      Lcd " C, "
      Locate 2 , 20
      Lcd Format_rh_string;
      Locate 2 , 26
      Lcd "%  "
   End If

   Reset Watchdog

   If Kommfehler2 = 1 Then                                  'fehlerbehandlung SHT71
      Locate 1 , 1
      Lcd "    SHT71 NC      "
   Else
      Locate 1 , 2
      Lcd Format_t_string2;
      Locate 1 , 8
      Lcd " C, "
      Locate 1 , 12
      Lcd Format_rh_string2;
      Locate 1 , 18
      Lcd "% "
   End If

   Reset Watchdog

   Locate 2 , 28
   If Laufvar1 = 1 Then                                     'Zustandszeichen
      Lcd "+"
   Else
      Lcd "-"
   End If
   Toggle Laufvar1

Return


' '------------  Unterroutine um ein Zeichen ans LCD zu schicken  ---------------------
Lcdwrite:
   ''Oberes Nibble setzen
   Lcd_d4 = Lcdbyte.4
   Lcd_d5 = Lcdbyte.5
   Lcd_d6 = Lcdbyte.6
   Lcd_d7 = Lcdbyte.7

   ''Daten abholen lassen
   Lcd_e = 1
   Waitus 100
   Lcd_e = 0
   Waitus 40

   ''Unteres Nibble setzen
   Lcd_d4 = Lcdbyte.0
   Lcd_d5 = Lcdbyte.1
   Lcd_d6 = Lcdbyte.2
   Lcd_d7 = Lcdbyte.3

   ''Daten abholen lassen
   Lcd_e = 1
   Waitus 100
   Lcd_e = 0
   Waitus 40

Return




'*************************** zus�tzliche BfS Sensoren Callbacks (SHT11 Bus) ************

'========================= I�C Befehlss�tze =======================

' '------------  Unterroutine um QIS Clk, Command, Address und Data zu generieren ---
Sub Write_i2c_qis(byval Qis_byte As Byte)
   Qis_sda_dir = 1
   For M = 7 To 0 Step -1
      Qis_clk = 0
      Qis_sda_out = Qis_byte.m
      Waitus 10
      Qis_clk = 1
      Waitus 10
   Next M
'   Qis_clk = 0
End Sub

' '------------  Unterroutine um QIS Clk, Command, Address und Data zu generieren ---
Sub Read_i2c_qis(qis_byte As Byte)
   Qis_sda_dir = 0
   Qis_sda_pull = 1
   For M = 7 To 0 Step -1
      Qis_clk = 0
      Waitus 10
      Qis_clk = 1
      Waitus 5
      Qis_byte.m = Qis_sda_in
   Next M
   Waitus 5
'   Qis_clk = 0
End Sub

' '------------  QIS I2C SHT reset communication ------------------------------------------------
Sht_reset_com:
   Qis_sda_dir = 1
   Qis_sda_out = 1
   Qis_clk = 0
   For M = 8 To 0 Step -1
      Qis_clk = 1
      Waitus 10
      Qis_clk = 0
      Waitus 10
   Next M
   Gosub Sht_transstart
Return

' '------------  QIS I2C SHT transition start condition ------------------------------------------------
Sht_transstart:
   Qis_sda_dir = 1
   Qis_sda_out = 1
   Qis_clk = 1
   Waitus 10
   Qis_sda_out = 0
   Waitus 10
   Qis_clk = 0
   Waitus 10
   Qis_clk = 1
   Waitus 10
   Qis_sda_out = 1
   Waitus 10
   Qis_clk = 0
   Waitus 10
Return

' '------------  QIS I2C start condition ------------------------------------------------
I2c_start_qis:
   Qis_sda_dir = 1
   Qis_sda_out = 1
   Waitus 10
   Qis_clk = 1
   Waitus 10
   Qis_sda_out = 0
Return

' '------------  QIS I2C write ack ------------------------------------------------------
I2c_write_ack_qis:
   Qis_clk = 0
   Qis_sda_dir = 1
   Qis_sda_out = 0
   Waitus 10
   Qis_clk = 1
   Waitus 10
   Qis_clk = 0
Return

' '------------  QIS I2C write nack ------------------------------------------------------
I2c_write_nack_qis:
   Qis_clk = 0
   Qis_sda_dir = 1
   Qis_sda_out = 1
   Waitus 10
   Qis_clk = 1
   Waitus 10
   Qis_clk = 0
Return

' '------------  QIS I2C read ack -------------------------------------------------------
Sub I2c_read_ack_qis(error As Byte)
   Qis_sda_dir = 0
   Qis_clk = 0
   Qis_sda_pull = 1
   Waitus 10
   Qis_clk = 1
   Waitus 10
   Error.0 = Qis_sda_in
   Qis_sda_dir = 1
   Qis_sda_out = 0
   Qis_clk = 0
End Sub

' '------------  QIS I2C stop condition -------------------------------------------------
I2c_stop_qis:
   Qis_sda_dir = 1
   Qis_sda_out = 0
   Waitus 10
   Qis_clk = 1
   Waitus 10
   Qis_sda_out = 1
Return


' '------------  SHT11 : set configuration register ---
Init_sht:
   'Print "SHT"
   Gosub Sht_transstart
   Call Write_i2c_qis(sht_st_w)
   Call I2c_read_ack_qis(i2c_error)
   If I2c_error.0 = 0 Then
      Call Write_i2c_qis(&H00)                              'high resolution
      Call I2c_read_ack_qis(i2c_error)
      If I2c_error.0 = 0 Then
         'Print "Init Successful"
      Else
         'Print "Init Failure 2"
      End If
   Else
      'Print "Init Failure 1"
   End If
   Qis_sda_dir = 1
   Qis_clk = 1
   Qis_sda_out = 1
Return

' '------------  SHT11 : reset
Reset_sht:
   'Print "Reset"
   Gosub Sht_transstart
   Call Write_i2c_qis(sht_reset)
   Call I2c_read_ack_qis(i2c_error)
   If I2c_error.0 = 0 Then
      'Print "Reset Successfull"
   Else
      'Print "Reset Failure"
   End If
   Qis_sda_dir = 1
   Qis_clk = 1
   Qis_sda_out = 1
Return




' '------------  SHT11 : read temp (braucht: Init_sht, Reset_sht, Sht_transstart, Write_i2c_qis, I2c_read_ack_qis)
Read_qis_sht11:

   Kommfehler = 0

   '* setze variablen auf 0 falls auslesefehler
   Sht_temp_high = 0
   Sht_temp_low = 0
   '* ende

   Gosub Init_sht
   Gosub Reset_sht
   Gosub Sht_transstart
   Call Write_i2c_qis(sht_temp)
   Call I2c_read_ack_qis(i2c_error)
   If I2c_error.0 = 0 Then
      Qis_sda_dir = 0
      Qis_sda_pull = 1
      Waitus 10
      Timeout_var = -32000
      While Qis_sda_in = 1 And Timeout_var < 32000          'warte maximal 160 ms
         Incr Timeout_var
         Waitus 3
      Wend
      If Timeout_var < 32000 Then
         'Print "SHT Readout"                                'debug
         Call Read_i2c_qis(sht_temp_high)
         Gosub I2c_write_ack_qis
         Call Read_i2c_qis(sht_temp_low)
         Gosub I2c_write_ack_qis
         Call Read_i2c_qis(sht_crc)
         Gosub I2c_write_nack_qis
         'Print Bin(sht_temp_high)                           'debug
         'Print Bin(sht_temp_low)                            'debug
         'Print "Readout Successfull"                        'debug
      Else
         Kommfehler = 1
         'Print "Readout Timeout"                            'debug
      End If
   Else
       Kommfehler = 1
      'Print "Readout Failure"                               'debug
   End If

   Qis_sda_dir = 1
   Qis_clk = 1
   Qis_sda_out = 1

   Sht_temp_out = Sht_temp_high                             'uncompressed temperature
   Shift Sht_temp_out , Left , 8
   Sht_temp_out = Sht_temp_out Or Sht_temp_low

   Sht_temp_single = Sht_temp_out * 0.01                    'conversion?
   Sht_temp_single = Sht_temp_single - 40

   '* Temperatur Endergebnis  (hier dann eine variable zuweisen)
   'Print "T = " ; Sht_temp_single
   Sht_out_single = Sht_temp_single
   T_string = Fusing(sht_out_single , "#.#")
   Format_t_string = Format(t_string , "+00000")
   'Print Format_t_string                                    'temperatur mit vorzeichen, drei stellen vorm komma und eine nachm komma


   '*humidity
   Gosub Init_sht
   Gosub Reset_sht
   Gosub Sht_transstart
   Call Write_i2c_qis(sht_rh)
   Call I2c_read_ack_qis(i2c_error)
   If I2c_error.0 = 0 Then
      Qis_sda_dir = 0
      Qis_sda_pull = 1
      Waitus 10
      Timeout_var = -32000
      While Qis_sda_in = 1 And Timeout_var < 32000
         Incr Timeout_var
         Waitus 3
      Wend
      If Timeout_var < 32000 Then
         'Print "SHT Readout"
         Call Read_i2c_qis(sht_temp_high)
         Gosub I2c_write_ack_qis
         Call Read_i2c_qis(sht_temp_low)
         Gosub I2c_write_ack_qis
         Call Read_i2c_qis(sht_crc)
         Gosub I2c_write_nack_qis
         'Print Bin(sht_temp_high)
         'Print Bin(sht_temp_low)
         'Print "Readout Successfull"
      Else
         Kommfehler = 1
         'Print "Readout Timeout"
      End If
   Else
      Kommfehler = 1
      'Print "Readout Failure"
   End If
   Qis_sda_dir = 1
   Qis_clk = 1
   Qis_sda_out = 1
   Sht_temp_out = Sht_temp_high                             'uncompressed humidity
   Shift Sht_temp_out , Left , 8
   Sht_temp_out = Sht_temp_out Or Sht_temp_low
   Single_tmp_i = 0.0367 * Sht_temp_out                     'conversion?
   Sht_humidity_single = Single_tmp_i - 2.0468
   Single_tmp_i = Sht_temp_out ^ 2
   Single_tmp_i = -1.5955e-6 * Single_tmp_i
   Sht_humidity_single = Sht_humidity_single + Single_tmp_i
   Sht_out_single = Sht_temp_single - 25                    'temperature compensation of humidity
   Single_tmp_i = 0.00008 * Sht_temp_out
   Single_tmp_i = Single_tmp_i + 0.01
   Sht_out_single = Sht_out_single * Single_tmp_i
   Sht_out_single = Sht_out_single + Sht_humidity_single

   If Sht_out_single < 0 Then
      Sht_out_single = 0
   End If
   '* Feuchtigkeit Endergebnis (hier eine variable zuweisen)
   'Print "rH = " ; Sht_humidity_single
   T_string = Fusing(sht_out_single , "#.##")
   Format_rh_string = Format(t_string , "000000")
   'Print Format_rh_string                                   'rel luftfeuchtigkeit, drei stellen vorm komma, zwei danach, kein vz

Return
' '------------  SHT11 : read temp Ende


'*************************** zus�tzliche BfS Sensoren Callbacks Ende (SHT11 Bus) *******




'*************************** zus�tzliche BfS Sensoren Callbacks (SHT71 Bus) ************

'========================= I�C Befehlss�tze =======================

' '------------  Unterroutine um QIS Clk, Command, Address und Data zu generieren ---
Sub Write_i2c_qis2(byval Qis_byte2 As Byte)
   Qis_sda_dir2 = 1
   For M = 7 To 0 Step -1
      Qis_clk2 = 0
      Qis_sda_out2 = Qis_byte2.m
      Waitus 10
      Qis_clk2 = 1
      Waitus 10
   Next M
'   Qis_clk = 0
End Sub

' '------------  Unterroutine um QIS Clk, Command, Address und Data zu generieren ---
Sub Read_i2c_qis2(qis_byte2 As Byte)
   Qis_sda_dir2 = 0
   Qis_sda_pull2 = 1
   For M = 7 To 0 Step -1
      Qis_clk2 = 0
      Waitus 10
      Qis_clk2 = 1
      Waitus 5
      Qis_byte2.m = Qis_sda_in2
   Next M
   Waitus 5
'   Qis_clk = 0
End Sub

' '------------  QIS I2C SHT reset communication ------------------------------------------------
Sht_reset_com2:
   Qis_sda_dir2 = 1
   Qis_sda_out2 = 1
   Qis_clk = 0
   For M = 8 To 0 Step -1
      Qis_clk2 = 1
      Waitus 10
      Qis_clk2 = 0
      Waitus 10
   Next M
   Gosub Sht_transstart2
Return

' '------------  QIS I2C SHT transition start condition ------------------------------------------------
Sht_transstart2:
   Qis_sda_dir2 = 1
   Qis_sda_out2 = 1
   Qis_clk2 = 1
   Waitus 10
   Qis_sda_out2 = 0
   Waitus 10
   Qis_clk2 = 0
   Waitus 10
   Qis_clk2 = 1
   Waitus 10
   Qis_sda_out2 = 1
   Waitus 10
   Qis_clk2 = 0
   Waitus 10
Return

' '------------  QIS I2C start condition ------------------------------------------------
I2c_start_qis2:
   Qis_sda_dir2 = 1
   Qis_sda_out2 = 1
   Waitus 10
   Qis_clk2 = 1
   Waitus 10
   Qis_sda_out2 = 0
Return

' '------------  QIS I2C write ack ------------------------------------------------------
I2c_write_ack_qis2:
   Qis_clk2 = 0
   Qis_sda_dir2 = 1
   Qis_sda_out2 = 0
   Waitus 10
   Qis_clk2 = 1
   Waitus 10
   Qis_clk2 = 0
Return

' '------------  QIS I2C write nack ------------------------------------------------------
I2c_write_nack_qis2:
   Qis_clk2 = 0
   Qis_sda_dir2 = 1
   Qis_sda_out2 = 1
   Waitus 10
   Qis_clk2 = 1
   Waitus 10
   Qis_clk2 = 0
Return

' '------------  QIS I2C read ack -------------------------------------------------------
Sub I2c_read_ack_qis2(error2 As Byte)
   Qis_sda_dir2 = 0
   Qis_clk2 = 0
   Qis_sda_pull2 = 1
   Waitus 10
   Qis_clk2 = 1
   Waitus 10
   Error2.0 = Qis_sda_in2
   Qis_sda_dir2 = 1
   Qis_sda_out2 = 0
   Qis_clk2 = 0
End Sub

' '------------  QIS I2C stop condition -------------------------------------------------
I2c_stop_qis2:
   Qis_sda_dir2 = 1
   Qis_sda_out2 = 0
   Waitus 10
   Qis_clk2 = 1
   Waitus 10
   Qis_sda_out2 = 1
Return


' '------------  SHT71 : set configuration register ---
Init_sht2:
   'Print "SHT"
   Gosub Sht_transstart2
   Call Write_i2c_qis2(sht_st_w)
   Call I2c_read_ack_qis2(i2c_error)
   If I2c_error.0 = 0 Then
      Call Write_i2c_qis2(&H00)                             'high resolution
      Call I2c_read_ack_qis2(i2c_error)
      If I2c_error.0 = 0 Then
         'Print "Init Successful"
      Else
         'Print "Init Failure 2"
      End If
   Else
      'Print "Init Failure 1"
   End If
   Qis_sda_dir2 = 1
   Qis_clk2 = 1
   Qis_sda_out2 = 1
Return

' '------------  SHT71 : reset
Reset_sht2:
   'Print "Reset"
   Gosub Sht_transstart2
   Call Write_i2c_qis2(sht_reset)
   Call I2c_read_ack_qis2(i2c_error)
   If I2c_error.0 = 0 Then
      'Print "Reset Successfull"
   Else
      'Print "Reset Failure"
   End If
   Qis_sda_dir2 = 1
   Qis_clk2 = 1
   Qis_sda_out2 = 1
Return




' '------------  SHT71 : read temp (braucht: Init_sht2, Reset_sht2, Sht_transstart2, Write_i2c_qis2, I2c_read_ack_qis2)
Read_qis_sht71:

   Kommfehler2 = 0

   '* setze variablen auf 0 falls auslesefehler
   Sht_temp_high = 0
   Sht_temp_low = 0
   '* ende

   Gosub Init_sht2
   Gosub Reset_sht2
   Gosub Sht_transstart2
   Call Write_i2c_qis2(sht_temp)
   Call I2c_read_ack_qis2(i2c_error)
   If I2c_error.0 = 0 Then
      Qis_sda_dir2 = 0
      Qis_sda_pull2 = 1
      Waitus 10
      Timeout_var = -32000
      While Qis_sda_in2 = 1 And Timeout_var < 32000         'warte maximal 160 ms
         Incr Timeout_var
         Waitus 3
      Wend
      If Timeout_var < 32000 Then
         'Print "SHT Readout"                                'debug
         Call Read_i2c_qis2(sht_temp_high)
         Gosub I2c_write_ack_qis2
         Call Read_i2c_qis2(sht_temp_low)
         Gosub I2c_write_ack_qis2
         Call Read_i2c_qis2(sht_crc)
         Gosub I2c_write_nack_qis2
         'Print Bin(sht_temp_high)                           'debug
         'Print Bin(sht_temp_low)                            'debug
         'Print "Readout Successfull"                        'debug
      Else
         Kommfehler2 = 1
         'Print "Readout Timeout"                            'debug
      End If
   Else
       Kommfehler2 = 1
      'Print "Readout Failure"                               'debug
   End If

   Qis_sda_dir2 = 1
   Qis_clk2 = 1
   Qis_sda_out2 = 1

   Sht_temp_out = Sht_temp_high                             'uncompressed temperature
   Shift Sht_temp_out , Left , 8
   Sht_temp_out = Sht_temp_out Or Sht_temp_low

   Sht_temp_single = Sht_temp_out * 0.01                    'conversion?
   Sht_temp_single = Sht_temp_single - 40

   '* Temperatur Endergebnis  (hier dann eine variable zuweisen)
   'Print "T = " ; Sht_temp_single
   Sht_out_single = Sht_temp_single
   T_string = Fusing(sht_out_single , "#.#")
   Format_t_string2 = Format(t_string , "+00000")
   'Print Format_t_string                                    'temperatur mit vorzeichen, drei stellen vorm komma und eine nachm komma


   '*humidity
   Gosub Init_sht2
   Gosub Reset_sht2
   Gosub Sht_transstart2
   Call Write_i2c_qis2(sht_rh)
   Call I2c_read_ack_qis2(i2c_error)
   If I2c_error.0 = 0 Then
      Qis_sda_dir2 = 0
      Qis_sda_pull2 = 1
      Waitus 10
      Timeout_var = -32000
      While Qis_sda_in2 = 1 And Timeout_var < 32000
         Incr Timeout_var
         Waitus 3
      Wend
      If Timeout_var < 32000 Then
         'Print "SHT Readout"
         Call Read_i2c_qis2(sht_temp_high)
         Gosub I2c_write_ack_qis2
         Call Read_i2c_qis2(sht_temp_low)
         Gosub I2c_write_ack_qis2
         Call Read_i2c_qis2(sht_crc)
         Gosub I2c_write_nack_qis2
         'Print Bin(sht_temp_high)
         'Print Bin(sht_temp_low)
         'Print "Readout Successfull"
      Else
         Kommfehler2 = 1
         'Print "Readout Timeout"
      End If
   Else
      Kommfehler2 = 1
      'Print "Readout Failure"
   End If
   Qis_sda_dir2 = 1
   Qis_clk2 = 1
   Qis_sda_out2 = 1
   Sht_temp_out = Sht_temp_high                             'uncompressed humidity
   Shift Sht_temp_out , Left , 8
   Sht_temp_out = Sht_temp_out Or Sht_temp_low
   Single_tmp_i = 0.0367 * Sht_temp_out                     'conversion?
   Sht_humidity_single = Single_tmp_i - 2.0468
   Single_tmp_i = Sht_temp_out ^ 2
   Single_tmp_i = -1.5955e-6 * Single_tmp_i
   Sht_humidity_single = Sht_humidity_single + Single_tmp_i
   Sht_out_single = Sht_temp_single - 25                    'temperature compensation of humidity
   Single_tmp_i = 0.00008 * Sht_temp_out
   Single_tmp_i = Single_tmp_i + 0.01
   Sht_out_single = Sht_out_single * Single_tmp_i
   Sht_out_single = Sht_out_single + Sht_humidity_single

   If Sht_out_single < 0 Then
      Sht_out_single = 0
   End If
   '* Feuchtigkeit Endergebnis (hier eine variable zuweisen)
   'Print "rH = " ; Sht_humidity_single
   T_string = Fusing(sht_out_single , "#.##")
   Format_rh_string2 = Format(t_string , "000000")
   'Print Format_rh_string                                   'rel luftfeuchtigkeit, drei stellen vorm komma, zwei danach, kein vz

Return
' '------------  SHT71 : read temp Ende


'*************************** zus�tzliche BfS Sensoren Callbacks Ende (SHT71 Bus) *******



Sendeversion:
Reset Watchdog
   Print "SHT0"
Return


Sendetemp:
Reset Watchdog
If Kommfehler2 = 0 Then
   Print Format_t_string2;
   Print Kommfehler2
Else
   Print Format_t_string;
   Print Kommfehler
End If
Return

Sendefeuchte:
Reset Watchdog
If Kommfehler2 = 0 Then
   Print Format_rh_string2;
   Print Kommfehler2
Else
   Print Format_rh_string;
   Print Kommfehler
End If
Return


Unbekannt:
Print "Unknown Command"
Return


Eingang:
   Disable Interrupts
   Reset Watchdog
   Input Befehl
   Select Case Befehl
      Case "V" : Gosub Sendeversion                         'Sendet den String 'SHT' zum PC
      Case "T" : Gosub Sendetemp                            'Temperatur von SHT71. Falls SHT71 Kommfehler, dann wird Temperatur von SHT11 genommen
      Case "F" : Gosub Sendefeuchte                         'Feuchte von SHT71. Falls SHT71 Kommfehler, dann wird Feuchte von SHT71 genommen
      Case Else : Gosub Unbekannt
   End Select
   Enable Interrupts
Return
